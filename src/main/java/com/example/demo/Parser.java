package com.example.demo;


import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Parser {

	private ScriptEngine engine;
	
	public void before() throws ScriptException {
		ScriptEngineManager sem = new ScriptEngineManager();
        this.engine = sem.getEngineByName("javascript");
        
        String json = "{\"name\":\"karthi\",\"arr\":[\"one\",\"two\"]}";
        String script = "Java.asJSONCompatible(" + json + ")";
        Object result = this.engine.eval(script);
        
        pojo p = (pojo) result;
        System.out.println(p.getName());
        
        Map contents = (Map) result;
        contents.forEach((key, value) -> {
        System.out.println(key+" : "+value);
        });
	}
	
	public static void main(String[] args) throws ScriptException {
		new Parser().before();
	}
}
