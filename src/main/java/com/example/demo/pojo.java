package com.example.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class pojo{
	
	
	private String[]  arr = new String[5];
	private String name;
	
	private List<String> marks;
	
	private ArrayList<String> ast;
	
	private Set<String> set;
	
	private sub subc;
	
	public pojo()
	{}

	public String[] getArr() {
		return arr;
	}

	public void setArr(String[] arr) {
		this.arr = arr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getMarks() {
		return marks;
	}

	public void setMarks(List<String> marks) {
		this.marks = marks;
	}

	public Set<String> getSet() {
		return set;
	}

	public void setSet(Set<String> set) {
		this.set = set;
	}

	public ArrayList<String> getAst() {
		return ast;
	}

	public void setAst(ArrayList<String> ast) {
		this.ast = ast;
	}

	public sub getSubc() {
		return subc;
	}

	public void setSubc(sub subc) {
		this.subc = subc;
	}
	
	
	
	
	
	
}
