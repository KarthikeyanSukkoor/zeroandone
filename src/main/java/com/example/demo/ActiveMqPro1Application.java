package com.example.demo;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
public class ActiveMqPro1Application {

	public static void main(String[] args) throws IllegalArgumentException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException {
		
		ArrayList<String> lst = new ArrayList<String>();
		lst.add("one");
		lst.add("two");
		
		Set<String> set = new HashSet<String>();
		lst.add("one");
		lst.add("two");
		
		
		pojo p = new pojo();
		p.setMarks(lst);
		p.setName("test");
		p.setSet(set);
		p.setAst(lst);
		
		p.getArr()[0] = "arrOne";
		
		JSONObject object = new JSONObject(p);
		//String[] keys = JSONObject.getNames(object);
		System.out.println(object);
		pojo p2 = (pojo) populateJavaObject(object, pojo.class);
		
		List<String> lt = p2.getMarks();
		
		for(String name : lt) {
			System.out.println(name);
		}
		
		
		ObjectMapper om = new ObjectMapper();
		om.readValue(src, valueType)
		
		System.out.println(p2.getName());
	}
	
	
	
	
	
	public static Object populateJavaObject(JSONObject json, Class class1) throws IllegalArgumentException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException {
        // TODO Auto-generated method stub
        Object obj = null;
        try {
            obj = class1.newInstance();           
            for (String key : JSONObject.getNames(json)) {                
                Object value = json.get(key);
                Field field = class1.getDeclaredField(key);
                field.setAccessible(true);
                if (value instanceof Integer) {
                    field.setInt(obj, (Integer)value);
                }else if(value instanceof JSONArray) {
                	
                	Class<?> claz = Class.forName(field.getType().getName());
                	Object list  = null;
                	
                	if(field.getType().equals(List.class)) {
                		claz = Class.forName("java.util.ArrayList");
                		list = claz.newInstance();
                	}else if(field.getType().equals(Set.class)) {
                		claz = Class.forName("java.util.HashSet");
                		list = claz.newInstance();
                	}else if(field.getType().isArray()) {
                		System.out.println("Array found");
                		list = claz.newInstance();
                	}
                	
                	System.out.println(field.getType());
                	
                	
                	
                	Method add = List.class.getDeclaredMethod("add",Object.class);

                	for(int i = 0; i < ((JSONArray)value).length(); i++) {
                		add.invoke(list, ((JSONArray)value).get(i));
                	}
                	             	
                	
                	field.set(obj, list);
                	
                	
                } else{
                	field.set(obj, value);
                    //field.setString(obj, (String)value);
                }
            }
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return obj;
    }
/*
    public static String getString(String jsonStr) {
        int i = index;
        StringBuffer buf = new StringBuffer();
        while (jsonStr.charAt(i) != '\"') {
            jsonStr.charAt(i);
            buf.append(jsonStr.charAt(i));
            i++;
        }
        index = i;
        return buf.toString();
    }

    public static JSONObject getJSONObject (String jsonStr) {
        StringBuffer buf = new StringBuffer();
        boolean isKey = true;
        String currentKey = "";
        Object currentValue = "";
        JSONObject json = new JSONObject();

        while (jsonStr.charAt(index) != '}') {
            if (jsonStr.charAt(index) == '\"') {
                index++;
                String token = getString(jsonStr);
                if (isKey) {
                    currentKey = token;
                } else {
                    currentValue = token;                       
                }
            } else if (Character.isDigit(jsonStr.charAt(index))) {
                Integer token = getNumber(jsonStr);
                currentValue = token;
            } else if (jsonStr.charAt(index) == '{') {
                currentValue = getJSONObject(jsonStr);
            } else if (jsonStr.charAt(index) == '[') {
                currentValue = getArray(jsonStr);
            } else if (jsonStr.charAt(index) == ':') {
                isKey = false;
            } else if (jsonStr.charAt(index) == ',' || jsonStr.charAt(index) == '}') {
                isKey = true;
                json.map.put(currentKey, currentValue);
            }
            index++;
        }

        return json;
    }

    private static ArrayList getArray(String jsonStr) {     
        ArrayList list = new ArrayList();
        while (jsonStr.charAt(index) != ']') {          
            index++;
        }
        return null;
    }

    private static Integer getNumber(String jsonStr) {
        // TODO Auto-generated method stub
        Integer num = 0;

        while (Character.isDigit(jsonStr.charAt(index))) {
            num = num * 10 + Integer.parseInt(jsonStr.charAt(index)+"");
            index++;
        }

        index--;

        return num;
    }

    public static Object parseJSON(String jsonStr) {
        Owner owner = new Owner();
        while (index <= jsonStr.length()) {
            if (jsonStr.charAt(index) == '{') {
                return getJSONObject(jsonStr);
            } else if (jsonStr.charAt(index) == '[') {
                return getArray(jsonStr);
            }
        }   

        return null;
    }

    public static String fetchJSON(String url) {
        String nextLine = "";
        try {
            URL sywURL = new URL(url);
            BufferedReader reader = new BufferedReader(new InputStreamReader(sywURL.openStream()));
            StringBuffer buf = new StringBuffer();          
            while ((nextLine = reader.readLine()) != null) {
                buf.append(nextLine);
            }
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return nextLine;
    }*/


}
